import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import IConversation from './interfaces/IConversation';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  activeConversation: IConversation;

  conversationSubject: Subject<IConversation> = new Subject<IConversation>();

  constructor(private http: HttpClient) {}

  getConversations(): Observable<any> {
    return this.http.get('assets/mock/conversations.json');
  }

  selectConversation(conversation: IConversation): void {
    this.activeConversation = conversation;
    this.conversationSubject.next(this.activeConversation);
  }
}
