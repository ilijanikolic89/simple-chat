import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewChecked,
  OnDestroy,
} from '@angular/core';
import { AppService } from './../../app.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import IConversation from './../../interfaces/IConversation';
import IMessage from './../../interfaces/IMessage';
import { groupBy } from 'lodash';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit, AfterViewChecked, OnDestroy {
  conversation: IConversation;
  componentDestroyed: Subject<boolean> = new Subject<boolean>();
  chatInput: string;

  @ViewChild('chatMessages') chatMessages: ElementRef;

  constructor(private appService: AppService) {}

  ngOnInit(): void {
    this.appService.conversationSubject
      .pipe(takeUntil(this.componentDestroyed))
      .subscribe((conversation) => {
        this.conversation = conversation;
      });
  }

  ngAfterViewChecked(): void {
    this.scrollChatToBottom();
  }

  scrollChatToBottom(): void {
    if (this.chatMessages) {
      this.chatMessages.nativeElement.scrollTop = this.chatMessages.nativeElement.scrollHeight;
    }
  }

  sendMessage(): void {
    if (this.chatInput.length > 0) {
      const newMessage: IMessage = {
        text: this.chatInput,
        time: new Date(),
        incoming: false,
      };
      this.conversation.messages.push(newMessage);
      this.chatInput = '';
    }
  }

  isDifferentDay(messageIndex: number): boolean {
    if (messageIndex === 0) {
      return true;
    }

    const d1 = new Date(this.conversation.messages[messageIndex - 1].time);
    const d2 = new Date(this.conversation.messages[messageIndex].time);

    return (
      d1.getFullYear() !== d2.getFullYear() ||
      d1.getMonth() !== d2.getMonth() ||
      d1.getDate() !== d2.getDate()
    );
  }

  getMessageDate(messageIndex: number): string {
    const today = new Date().toDateString();
    const longDateYesterday = new Date();
    longDateYesterday.setDate(new Date().getDate() - 1);
    const yesterday = longDateYesterday.toDateString();

    const messageDate = new Date(
      this.conversation.messages[messageIndex].time
    ).toDateString();

    if (messageDate === today) {
      return 'Today';
    } else if (messageDate === yesterday) {
      return 'Yesterday';
    } else {
      // This returns only date, without day name
      return messageDate.slice(4, messageDate.length);
    }
  }

  ngOnDestroy(): void {
    this.componentDestroyed.next(true);
  }
}
