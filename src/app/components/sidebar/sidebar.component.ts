import { Component, OnInit } from '@angular/core';
import { AppService } from './../../app.service';
import IConversation from 'src/app/interfaces/IConversation';
import IMessage from './../../interfaces/IMessage';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  conversations: Array<IConversation> = [];

  constructor(private appService: AppService) {}

  ngOnInit(): void {
    this.appService.getConversations().subscribe((conversations) => {
      this.conversations = conversations;
      this.selectConversation(conversations[0]);
    });
  }

  lastMessage(conversation: IConversation): IMessage {
    return conversation.messages[conversation.messages.length - 1];
  }

  selectConversation(conversation: IConversation): void {
    this.appService.selectConversation(conversation);
  }

  get activeConversation(): IConversation {
    return this.appService.activeConversation;
  }
}
