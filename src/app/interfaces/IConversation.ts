import IMessage from './IMessage';

export default interface IConversation {
  id: number;
  user: string;
  messages: Array<IMessage>;
}
