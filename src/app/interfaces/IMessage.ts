export default interface IMessage {
  text: string;
  time: Date;
  incoming: boolean;
}
